import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'onekdoti.settings'
import django
django.setup()
from anekdoti.models import Rubric

rubr = Rubric.objects.filter(approved=True)
urls=''
for rub in rubr:
    urls += '<url>\n<loc>'+'http://onekdoti.ru/'+rub.url+'</loc>\n</url>\n'

sitemap = open('static/sitemap.xml', 'w')
sitemap.write('<?xml version="1.0" encoding="UTF-8"?>\n<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'+"\n"+urls+'</urlset>')
sitemap.close()