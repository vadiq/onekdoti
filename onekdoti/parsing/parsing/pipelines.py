# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'onekdoti.settings'
import django

django.setup()
import re
from bs4 import BeautifulSoup
from anekdoti.models import Anekdots, Rubric

# class BasePipeline(object):
#     def process_item(self, item, spider):
#         rub = item['rubric'][-1]
#         rubrik = BeautifulSoup(rub)
#         rubr = rubrik.string
#         rubricz = rubr
#         rubrik, znach = Rubric.objects.get_or_create(name=rubricz)
#         item['rubric'] = rubrik
#         for j in item['text']:
#             pattern = r"</center><br>.+<br><br></td>"
#             new = j.split("\r")
#             new = " ".join(new)
#             new = new.split("\n")
#             new = " ".join(new)
#             text = re.findall(pattern, new)
#             tex = text[0][13:(len(text[0])-13)]
#             item['text'] = tex
#             Anekdots(text=tex, rubric=rubrik, url=item['url']).save()


# class BasePipeline(object):
#     def process_item(self, item, spider):
#         rub = item['rubric'][0]
#         rubrik = rub[4:(len(rub)-5)]
#         if 'анекдоты от' in rubrik:
#             rubrik = Rubric.objects.get(id=1013)
#         else:
#             rubrik, znach = Rubric.objects.get_or_create(name=rubrik)
#         item['rubric'] = rubrik
#         for j in item['text'][:15]:
#             if "div" in j:
#                 tex = j[21:(len(j)-6)]
#             else:
#                 tex = j[19:(len(j) - 4)]
#             if '<a' in tex:
#                 p = re.compile(r'<a.*?>|</a>')
#                 k = p.sub('', tex)
#                 tex = k
#                 # k = tex.split('</a>')
#                 # tex = "".join(k)
#                 # k = tex.split('<a.+>')
#                 # tex = "".join(k)
#             item['text'] = tex
#             Anekdots(text=tex, rubric=rubrik, url=item['url']).save()


class BasePipeline(object):
    def process_item(self, item, spider):
        rub = item['rubric'][0]
        rubrik = rub[4:(len(rub)-5)]
        rubrik, znach = Rubric.objects.get_or_create(name=rubrik)
        item['rubric'] = rubrik
        for j in item['text']:
            tex = j[18:(len(j)-6)]
            if '<a' in tex:
                p = re.compile(r'<a.*?>|</a>')
                k = p.sub('', tex)
                tex = k
                # k = tex.split('</a>')
                # tex = "".join(k)
                # k = tex.split('<a.+>')
                # tex = "".join(k)
            item['text'] = tex
            Anekdots(text=tex, rubric=rubrik, url=item['url']).save()