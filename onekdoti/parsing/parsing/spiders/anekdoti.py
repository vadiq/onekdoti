import scrapy
# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from parsing.items import AnekdotsItem

# class AnekdotiSpider(CrawlSpider):
#     name = "anekdoti"
#     allowed_domains = ["shytok.net"]
#     start_urls = [
#         "http://www.shytok.net/"
#     ]
#
#     rules = (
#         Rule(LinkExtractor(allow=('anekdots/.+')), callback='parse_item', follow=True),
#     )
#
#     def parse_item(self, response):
#         self.logger.info('This is an item page! %s', response.url)
#         try:
#             item = AnekdotsItem()
#             item['url'] = response.url
#             item['rubric'] = response.css('[rel="v:url"]').extract()
#             item['text'] = response.css('[width="90%"]').extract()
#         except Exception, e:
#             self.logger.error(str(e))
#             raise e
#
#         return item


# class AneknetSpider(CrawlSpider):
#     name = "aneknet"
#     allowed_domains = ["anekdotov.net"]
#     start_urls = [
#         "http://anekdotov.net/intim",
#         "http://anekdotov.net/anwar",
#         "http://anekdotov.net/ancomp",
#         "http://anekdotov.net/nr",
#         "http://anekdotov.net/sms"
#     ]
#
#     rules = (
#         Rule(LinkExtractor(allow=('intim/.+', 'anwar/.+', 'ancomp/.+', 'nr/.+', 'sms/.+')), callback='parse_item', follow=True),
#         # Rule(LinkExtractor(allow=('vovochka/.+', 'intim/.+', 'anwar/.+', 'ancomp/.+', 'nr/.+', 'sms/.+'), deny=('anekdot/today', 'anekdot/day', 'anekdot/week', 'anekdot/month', 'anekdot/form', 'anekdot/new')), callback='parse_item', follow=True),
#         # Rule(LinkExtractor(allow=('anekdot/.+'), deny=('anekdot/today\.html', )), callback='parse_item', follow=True),
#         # Rule(LinkExtractor(allow=('http://anekdotov.net/anekdot/today.html')), callback='parse_item', follow=True),
#     )
#
#     def parse_item(self, response):
#         self.logger.info('This is an item page! %s', response.url)
#         try:
#             item = AnekdotsItem()
#             item['url'] = response.url
#             item['rubric'] = response.css('h1').extract()
#             item['text'] = response.css('[align="justify"]').extract()
#         except Exception, e:
#             self.logger.error(str(e))
#             raise e
#
#         return item



class AnekdotSpider(CrawlSpider):
    name = "anekdot"
    allowed_domains = ["anekdot.ru"]
    start_urls = [
        "http://www.anekdot.ru/tags/"
    ]

    rules = (
        Rule(LinkExtractor(allow=('tags/.+')), callback='parse_item', follow=True),
    )

    def parse_item(self, response):
        self.logger.info('This is an item page! %s', response.url)
        try:
            item = AnekdotsItem()
            item['url'] = response.url
            item['rubric'] = response.css('h1').extract()
            item['text'] = response.css('[class="text"]').extract()
        except Exception, e:
            self.logger.error(str(e))
            raise e

        return item