var app = angular.module('menu', []);

app.controller('MenuController', function(){
    this.tab = 1;

    this.selectTab = function(setTab) {
        this.tab = setTab;
    };
    this.isSelected = function(checkTab) {
        return this.tab === checkTab;
    };

    this.selectTabRubric = function(setTab) {
        this.tab = setTab;
    };
    this.isSelectedRubric = function(checkTab) {
        return this.tab === checkTab;
    };
});


app.controller('RubricController', function(){
    this.tab = 1;

    this.selectTabRubric = function(setTab) {
        this.tab = setTab;
    };
    this.isSelectedRubric = function(checkTab) {
        return this.tab === checkTab;
    };
});