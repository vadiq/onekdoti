function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$(".add_korzina").click(function(e) {
    var id = this.id;
    e.preventDefault();
    var csrftoken = getCookie('csrftoken');
    var anekdot_id = id;

//This is the Ajax post.Observe carefully. It is nothing but details of where_to_post,what_to_post

 $.ajax({
 url : "/add-to-korzina/", // the endpoint,commonly same url
 type : "POST", // http method
 data : { csrfmiddlewaretoken : csrftoken,
 anekdot_id : anekdot_id,
 }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            console.log(json); // another sanity check
            //alert(json['added']);
            $(document.getElementById('anek_korzina'+id)).text(json['added']);
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            alert("Вы уже добавили анекдот в корзину")
            //console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
});

$(".remove_korzina").click(function(e) {
    var id = this.id;
    e.preventDefault();
    var csrftoken = getCookie('csrftoken');
    var anekdot_id = id;

//This is the Ajax post.Observe carefully. It is nothing but details of where_to_post,what_to_post

 $.ajax({
 url : "/remove-korzina/", // the endpoint,commonly same url
 type : "POST", // http method
 data : { csrfmiddlewaretoken : csrftoken,
 anekdot_id : anekdot_id,
 }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            console.log(json); // another sanity check
            //alert(json['removed']);
            $(document.getElementById('anek_korzina'+id)).text(json['removed']);
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            alert("Вы уже добавили анекдот в корзину")
            //console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
});

$(".add_mytop").click(function(e) {
    var id = this.id;
    e.preventDefault();
    var csrftoken = getCookie('csrftoken');
    var anekdot_id = id;

//This is the Ajax post.Observe carefully. It is nothing but details of where_to_post,what_to_post

 $.ajax({
 url : "/add-to-top/", // the endpoint,commonly same url
 type : "POST", // http method
 data : { csrfmiddlewaretoken : csrftoken,
 anekdot_id : anekdot_id,
 }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            console.log(json); // another sanity check
            //alert(json['added']);
            $(document.getElementById('anek_korzina'+id)).text(json['added']);
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            alert("Вы не можете добавить в ТОР более 9 анекдотов. Можете из ТОП-а убрать какой-то анекдот, и затем добавить новый.")
            //console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
});

$(".remove_mytop").click(function(e) {
    var id = this.id;
    e.preventDefault();
    var csrftoken = getCookie('csrftoken');
    var anekdot_id = id;

//This is the Ajax post.Observe carefully. It is nothing but details of where_to_post,what_to_post

 $.ajax({
 url : "/remove-top/", // the endpoint,commonly same url
 type : "POST", // http method
 data : { csrfmiddlewaretoken : csrftoken,
 anekdot_id : anekdot_id,
 }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            console.log(json); // another sanity check
            //alert(json['removed']);
            $(document.getElementById('anek_korzina'+id)).text(json['removed']);
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            alert("Вы уже добавили анекдот в корзину")
            //console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
});

$(".like").click(function(e) {
    var id = this.id;
    e.preventDefault();
    var csrftoken = getCookie('csrftoken');
    var anekdot_id = id;

    $.ajax({
         url : "/like/",
         type : "POST",
         data : { csrfmiddlewaretoken : csrftoken,
         anekdot_id : anekdot_id,
         },

        success : function(json) {
            console.log(json);
            //alert(json['vot']);
            $(document.getElementById('anek'+id)).text(json['new_rating']);
        },

        error : function(xhr,errmsg,err) {
            alert("Вы уже проголосовали за данный анекдот")
        }
    });
});


$(".unlike").click(function(e) {
    var id = this.id;
    e.preventDefault();
    var csrftoken = getCookie('csrftoken');
    var anekdot_id = id;

    $.ajax({
         url : "/unlike/",
         type : "POST",
         data : { csrfmiddlewaretoken : csrftoken,
         anekdot_id : anekdot_id,
         },

        success : function(json) {
            console.log(json);
            //alert(json['vot']);
            $(document.getElementById('anek'+id)).text(json['new_rating']);
        },

        error : function(xhr,errmsg,err) {
            alert("Вы уже проголосовали за данный анекдот")
        }
    });
});


$("#login").click(function(e) {
    var login = document.getElementById('username').value;
    var password = document.getElementById('password').value;
    e.preventDefault();
    var csrftoken = getCookie('csrftoken');

    $.ajax({
         url : "/login/",
         type : "POST",
         data : { csrfmiddlewaretoken : csrftoken,
         login: login,
         password : password,
         },

        success : function(json) {
            console.log(json);
            window.location.replace("http://127.0.0.1:8000/");
            //alert(json['vot']);
            // $(document.getElementById('anek'+id)).text(json['new_rating']);
        },

        error : function(xhr,errmsg,err) {
            $(document.getElementById('error')).text('Пользователь не найден или пароль не совпадает');
        }
    });
});

$(".like-collect").click(function(e) {
    var id = this.id;
    e.preventDefault();
    var csrftoken = getCookie('csrftoken');
    var collect_id = id;

    $.ajax({
         url : "/like-collect/",
         type : "POST",
         data : { csrfmiddlewaretoken : csrftoken,
         collect_id : collect_id,
         },

        success : function(json) {
            console.log(json);
            //alert(json['vot']);
            $(document.getElementById('kolek'+id)).text(json['new_rating']);
        },

        error : function(xhr,errmsg,err) {
            alert("Вы уже проголосовали за данную коллекцию анекдотов")
        }
    });
});


$(".unlike-collect").click(function(e) {
    var id = this.id;
    e.preventDefault();
    var csrftoken = getCookie('csrftoken');
    var collect_id = id;

    $.ajax({
         url : "/unlike-collect/",
         type : "POST",
         data : { csrfmiddlewaretoken : csrftoken,
         collect_id : collect_id,
         },

        success : function(json) {
            console.log(json);
            //alert(json['vot']);
            $(document.getElementById('kolek'+id)).text(json['new_rating']);
        },

        error : function(xhr,errmsg,err) {
            alert("Вы уже проголосовали за данную коллекцию анекдотов")
        }
    });
});

$(".like-top9").click(function(e) {
    var id = this.id;
    e.preventDefault();
    var csrftoken = getCookie('csrftoken');
    var top9_id = id;

    $.ajax({
         url : "/like-top9/",
         type : "POST",
         data : { csrfmiddlewaretoken : csrftoken,
         top9_id : top9_id,
         },

        success : function(json) {
            console.log(json);
            //alert(json['vot']);
            $(document.getElementById('top9'+id)).text(json['new_rating']);
        },

        error : function(xhr,errmsg,err) {
            alert("Вы уже проголосовали за данный ТОП-9")
        }
    });
});


$(".unlike-top9").click(function(e) {
    var id = this.id;
    e.preventDefault();
    var csrftoken = getCookie('csrftoken');
    var top9_id = id;

    $.ajax({
         url : "/unlike-top9/",
         type : "POST",
         data : { csrfmiddlewaretoken : csrftoken,
         top9_id : top9_id,
         },

        success : function(json) {
            console.log(json);
            //alert(json['vot']);
            $(document.getElementById('top9'+id)).text(json['new_rating']);
        },

        error : function(xhr,errmsg,err) {
            alert("Вы уже проголосовали за данный ТОП-9")
        }
    });
});