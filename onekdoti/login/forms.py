# -*- coding: utf-8 -*-
from django.db import models
from django import forms
from django.contrib.auth import password_validation
from . models import User

class UserCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'password_mismatch': ("Пароли должны совпадать"),
    }
    password1 = forms.CharField(label=("Пароль"),
        strip=False,
        widget=forms.PasswordInput)
    password2 = forms.CharField(label=("Подтверждение пароля"),
        widget=forms.PasswordInput,
        strip=False,
        help_text=("Введите пароль повторно"))

    class Meta:
        model = User
        fields = ("username", "email")

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        self.instance.username = self.cleaned_data.get('username')
        password_validation.validate_password(self.cleaned_data.get('password2'), self.instance)
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


# class UserCreationForma(UserCreationForm):
#     class Meta:
#         model = User
#         fields = ("username", "email")