from django.contrib import admin
from . models import Collections, User, MyTop

admin.site.register(Collections)
admin.site.register(MyTop)
admin.site.register(User)