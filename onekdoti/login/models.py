# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core import validators
from django.utils import timezone
from django.core.mail import send_mail
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager
from anekdoti.models import Anekdots, Rubric


class AbstractUser(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username and password are required. Other fields are optional.
    """
    username = models.CharField(
        ('Логин'),
        max_length=30,
        unique=True,
        help_text=('Максимум 30 символов'),
        validators=[
            validators.RegexValidator(
                r'^[\w.@+-]+$',
                ('Введите доступные символы (анлийские буквы, цифры, знаки подчеркивания')
            ),
        ],
        error_messages={
            'unique': ("Пользователь с данным именем уже существует"),
        },
    )
    first_name = models.CharField(('first name'), max_length=30, blank=True)
    last_name = models.CharField(('last name'), max_length=30, blank=True)

    email = models.EmailField(('е-майл'),
                              unique=True,
                              error_messages={
                                  'unique': ("Пользователь с данным е-майлом уже существует"),
                              },
                              )


    is_staff = models.BooleanField(
        ('staff status'),
        default=False,
        help_text=('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        ('active'),
        default=True,
        help_text=(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(('date joined'), default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = ('user')
        verbose_name_plural = ('users')
        abstract = True

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)


class User(AbstractUser):
    """
    Users within the Django authentication system are represented by this
    model.

    Username, password and email are required. Other fields are optional.
    """
    pass


class Collections(models.Model):
    user = models.ForeignKey(User)
    anekdotki = models.ManyToManyField(Anekdots, blank=True)
    rating = models.IntegerField(default=0)
    liked_ip = models.TextField(blank=True)

class MyTop(models.Model):
    user = models.ForeignKey(User)
    anekdotki = models.ManyToManyField(Anekdots, blank=True)
    rating = models.IntegerField(default=0)
    liked_ip = models.TextField(blank=True)