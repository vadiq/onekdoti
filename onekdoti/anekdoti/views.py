# -*- coding: utf-8 -*-
import random
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.db.models import Q
from django.contrib import auth
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from . models import Anekdots, Rubric, Menu
from login.forms import UserCreationForm
from login.models import Collections, MyTop, User


def anekdoti(request):
    anekdoti = Anekdots.objects.filter(rubric__approved=True)
    paginator = Paginator(anekdoti, 20)
    page = request.GET.get('page')
    try:
        anekdots_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        anekdots_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        anekdots_page = paginator.page(paginator.num_pages)
    context = {'username': auth.get_user(request), 'anekdots_page': anekdots_page}
    return render(request, 'main.html', context)


def rubric(request, rubric):
    anekdoti = Anekdots.objects.filter(rubric__url=rubric)
    rubric = Rubric.objects.filter(url=rubric).first()
    paginator = Paginator(anekdoti, 20)
    page = request.GET.get('page')
    try:
        anekdots_page = paginator.page(page)
    except PageNotAnInteger:
        anekdots_page = paginator.page(1)
    except EmptyPage:
        anekdots_page = paginator.page(paginator.num_pages)
    context = {'username': auth.get_user(request), 'anekdots_page': anekdots_page, 'rubric': rubric}
    return render(request, 'rubric.html', context)


def menu(request, menu):
    menuinfo = Menu.objects.get(url=menu)
    return render(request, 'menu.html', {'menuinfo': menuinfo})


def login(request):
    args = {}
    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect('/')
        else:
            data = {"error_login": 'Пользователь не найден или пароль не совпадает'}
            return JsonResponse(data)
            # args['login_error'] = 'Пользователь не найден или пароль не совпадает'
            # return render(request, 'login.html', args)
    else:
        return render(request, 'login.html', args)

def logout(request):
    auth.logout(request)
    return redirect('/')

def register(request):
    args = {}
    args['form'] = UserCreationForm
    if request.POST:
        newuser_form = UserCreationForm(request.POST)
        if newuser_form.is_valid():
            newuser_form.save()
            newuser = auth.authenticate(username=newuser_form.cleaned_data['username'],
                                        password=newuser_form.cleaned_data['password1'])
            auth.login(request, newuser)
            Collections.objects.create(user=auth.get_user(request))
            MyTop.objects.create(user=auth.get_user(request))
            return redirect('/')
        else:
            args['form'] = newuser_form
    return render(request, 'register.html', args)

def add_korzina(request):
    # anekdoti = Anekdots.objects.all()[0]
    # k, r = Collections.objects.get_or_create(user=auth.get_user(request))
    # k.anekdotki.add(anekdoti)
    # return render(request, 'main.html')
    if request.method == 'POST':
        # POST goes here . is_ajax is must to capture ajax requests. Beginners pit.
        if request.is_ajax():
            id = request.POST.get('anekdot_id')
            anek = Anekdots.objects.get(id=id)
            k, r = Collections.objects.get_or_create(user=auth.get_user(request))
            k.anekdotki.add(anek)
            added = 'Анекдот добавлен в корзину'
            data = {"added": added}
            return JsonResponse(data)

def remove_korzina(request):
    if request.method == 'POST':
        if request.is_ajax():
            id = request.POST.get('anekdot_id')
            anek = Anekdots.objects.get(id=id)
            k, r = Collections.objects.get_or_create(user=auth.get_user(request))
            k.anekdotki.remove(anek)
            removed = 'Анекдот удален из коризны'
            data = {"removed": removed}
            return JsonResponse(data)

def add_my_top(request):
    if request.method == 'POST':
        if request.is_ajax():
            id = request.POST.get('anekdot_id')
            anek = Anekdots.objects.get(id=id)
            k, r = MyTop.objects.get_or_create(user=auth.get_user(request))
            if k.anekdotki.count() < 9:
                k.anekdotki.add(anek)
                added = 'Анекдот добавлен в ТОП'
                data = {"added": added}
            return JsonResponse(data)

def remove_my_top(request):
    if request.method == 'POST':
        if request.is_ajax():
            id = request.POST.get('anekdot_id')
            anek = Anekdots.objects.get(id=id)
            k, r = MyTop.objects.get_or_create(user=auth.get_user(request))
            k.anekdotki.remove(anek)
            removed = 'Анекдот удален из ТОП-а'
            data = {"removed": removed}
            return JsonResponse(data)

def my_anekdots(request):
    kolektia = Collections.objects.get(user=auth.get_user(request))
    anekdoti = Anekdots.objects.filter(collections=kolektia)
    paginator = Paginator(anekdoti, 20)
    page = request.GET.get('page')
    try:
        anekdots_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        anekdots_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        anekdots_page = paginator.page(paginator.num_pages)
    context = {'username': auth.get_user(request), 'anekdots_page': anekdots_page}
    return render(request, 'myaccount.html', context)

def my_top_anekdots(request):
    my_top = MyTop.objects.get(user=auth.get_user(request))
    anekdoti = Anekdots.objects.filter(mytop=my_top)
    paginator = Paginator(anekdoti, 20)
    page = request.GET.get('page')
    try:
        anekdots_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        anekdots_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        anekdots_page = paginator.page(paginator.num_pages)
    context = {'username': auth.get_user(request), 'anekdots_page': anekdots_page}
    return render(request, 'mytopanekdots.html', context)

def like(request):
    if request.method == 'POST':
        if request.is_ajax():
            id = request.POST.get('anekdot_id')
            ip = request.META.get('REMOTE_ADDR')
            anek = Anekdots.objects.get(id=id)
            if ":%sup" % ip not in anek.liked_ip:
                new_rating = anek.rating + 1
                anek.rating = new_rating
                anek.liked_ip += ":%sup" % ip
                anek.save()
            if ":%sdown" % ip in anek.liked_ip:
                anek.liked_ip = anek.liked_ip.split(":%sdown" % ip)
                anek.liked_ip = "".join(anek.liked_ip)
                anek.liked_ip = anek.liked_ip.split(":%sup" % ip)
                anek.liked_ip = "".join(anek.liked_ip)
                anek.save()
            data = {"id": id, "new_rating": new_rating}
            return JsonResponse(data)


def unlike(request):
    if request.method == 'POST':
        if request.is_ajax():
            id = request.POST.get('anekdot_id')
            ip = request.META.get('REMOTE_ADDR')
            anek = Anekdots.objects.get(id=id)
            if ":%sdown" % ip not in anek.liked_ip:
                new_rating = anek.rating - 1
                anek.rating = new_rating
                anek.liked_ip += ":%sdown" % ip
                anek.save()
            if ":%sup" % ip in anek.liked_ip:
                anek.liked_ip = anek.liked_ip.split(":%sup" % ip)
                anek.liked_ip = "".join(anek.liked_ip)
                anek.liked_ip = anek.liked_ip.split(":%sdown" % ip)
                anek.liked_ip = "".join(anek.liked_ip)
                anek.save()
            data = {"id": id, "new_rating": new_rating}
            return JsonResponse(data)



def login_ajax(request):
    username = request.POST.get('login', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username=username, password=password)
    if user is not None:
        auth.login(request, user)
        return redirect('/')
        data = {"error_login": 'Пользователь не найден или пароль не совпадает'}
    return JsonResponse(data)

def search(request):
    query = request.GET['search-text']
    anekdoti = Anekdots.objects.filter(Q(text__icontains=query) | Q(rubric__name__icontains=query), rubric__approved=True)
    paginator = Paginator(anekdoti, 20)
    page = request.GET.get('page')
    try:
        anekdots_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        anekdots_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        anekdots_page = paginator.page(paginator.num_pages)
    context = {'anekdots_page': anekdots_page, 'query': query}
    return render(request, 'search.html', context)

def page_anekdot(request, anekdor_id):
    anekdot = Anekdots.objects.filter(id=anekdor_id)[0]
    context = {'anekdot': anekdot}
    return render(request, 'anekdot.html', context)


def user_collect(request, user_collect):
    user_col = User.objects.get(username=user_collect)
    kolektia = Collections.objects.get(user=user_col)
    anekdoti = Anekdots.objects.filter(collections=kolektia)
    paginator = Paginator(anekdoti, 20)
    page = request.GET.get('page')
    try:
        anekdots_page = paginator.page(page)
    except PageNotAnInteger:
        anekdots_page = paginator.page(1)
    except EmptyPage:
        anekdots_page = paginator.page(paginator.num_pages)
    context = {'anekdots_page': anekdots_page, 'kolektia': kolektia}
    return render(request, 'user_collect.html', context)

def user_top9(request, user_top9):
    user_col = User.objects.get(username=user_top9)
    mytop9 = MyTop.objects.get(user=user_col)
    anekdoti = Anekdots.objects.filter(mytop=mytop9)
    paginator = Paginator(anekdoti, 20)
    page = request.GET.get('page')
    try:
        anekdots_page = paginator.page(page)
    except PageNotAnInteger:
        anekdots_page = paginator.page(1)
    except EmptyPage:
        anekdots_page = paginator.page(paginator.num_pages)
    context = {'anekdots_page': anekdots_page, 'mytop9': mytop9}
    return render(request, 'user_top9.html', context)

def like_collect(request):
    if request.method == 'POST':
        if request.is_ajax():
            id = request.POST.get('collect_id')
            ip = request.META.get('REMOTE_ADDR')
            coll = Collections.objects.get(id=id)
            if ":%sup" % ip not in coll.liked_ip:
                new_rating = coll.rating + 1
                coll.rating = new_rating
                coll.liked_ip += ":%sup" % ip
                coll.save()
            if ":%sdown" % ip in coll.liked_ip:
                coll.liked_ip = coll.liked_ip.split(":%sdown" % ip)
                coll.liked_ip = "".join(coll.liked_ip)
                coll.liked_ip = coll.liked_ip.split(":%sup" % ip)
                coll.liked_ip = "".join(coll.liked_ip)
                coll.save()
            data = {"id": id, "new_rating": new_rating}
            return JsonResponse(data)


def unlike_collect(request):
    if request.method == 'POST':
        if request.is_ajax():
            id = request.POST.get('collect_id')
            ip = request.META.get('REMOTE_ADDR')
            coll = Collections.objects.get(id=id)
            if ":%sdown" % ip not in coll.liked_ip:
                new_rating = coll.rating - 1
                coll.rating = new_rating
                coll.liked_ip += ":%sdown" % ip
                coll.save()
            if ":%sup" % ip in coll.liked_ip:
                coll.liked_ip = coll.liked_ip.split(":%sup" % ip)
                coll.liked_ip = "".join(coll.liked_ip)
                coll.liked_ip = coll.liked_ip.split(":%sdown" % ip)
                coll.liked_ip = "".join(coll.liked_ip)
                coll.save()
            data = {"id": id, "new_rating": new_rating}
            return JsonResponse(data)


def like_top9(request):
    if request.method == 'POST':
        if request.is_ajax():
            id = request.POST.get('top9_id')
            ip = request.META.get('REMOTE_ADDR')
            top9 = MyTop.objects.get(id=id)
            if ":%sup" % ip not in top9.liked_ip:
                new_rating = top9.rating + 1
                top9.rating = new_rating
                top9.liked_ip += ":%sup" % ip
                top9.save()
            if ":%sdown" % ip in top9.liked_ip:
                top9.liked_ip = top9.liked_ip.split(":%sdown" % ip)
                top9.liked_ip = "".join(top9.liked_ip)
                top9.liked_ip = top9.liked_ip.split(":%sup" % ip)
                top9.liked_ip = "".join(top9.liked_ip)
                top9.save()
            data = {"id": id, "new_rating": new_rating}
            return JsonResponse(data)


def unlike_top9(request):
    if request.method == 'POST':
        if request.is_ajax():
            id = request.POST.get('top9_id')
            ip = request.META.get('REMOTE_ADDR')
            top9 = MyTop.objects.get(id=id)
            if ":%sdown" % ip not in top9.liked_ip:
                new_rating = top9.rating - 1
                top9.rating = new_rating
                top9.liked_ip += ":%sdown" % ip
                top9.save()
            if ":%sup" % ip in top9.liked_ip:
                top9.liked_ip = top9.liked_ip.split(":%sup" % ip)
                top9.liked_ip = "".join(top9.liked_ip)
                top9.liked_ip = top9.liked_ip.split(":%sdown" % ip)
                top9.liked_ip = "".join(top9.liked_ip)
                top9.save()
            data = {"id": id, "new_rating": new_rating}
            return JsonResponse(data)

def all_rubrics(request):
    rubrics = Rubric.objects.filter(approved=True)

    def sort_by_count_anekdots(rubr_count_anekdots):
        return len(Anekdots.objects.filter(rubric=rubr_count_anekdots))

    rubrics = sorted(rubrics, key=sort_by_count_anekdots, reverse=True)
    rubrics = rubrics
    best_anekdots_in_rubric = []
    anectods_rubric_count = []
    count_for_background = []

    for rubric in rubrics:
        best_anekdot = Anekdots.objects.filter(rubric=rubric).order_by('-rating')[0]
        best_anekdots_in_rubric.append(best_anekdot)
        anectods_rubric_count.append(len(Anekdots.objects.filter(rubric=rubric)))
        count_for_background.append(random.randint(1, 8))

    all_rubrics = zip(rubrics, best_anekdots_in_rubric, anectods_rubric_count, count_for_background)

    context = {'all_rubrics': all_rubrics}
    return render(request, 'all_rubrics.html', context)