from __future__ import unicode_literals

from django.apps import AppConfig


class AnekdotiConfig(AppConfig):
    name = 'anekdoti'
