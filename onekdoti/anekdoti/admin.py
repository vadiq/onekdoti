from django.contrib import admin

from . models import Rubric, Anekdots, Site, Menu


class AnekdotiAdmin(admin.ModelAdmin):
    fieldsets = [
        ('General info', {'fields': ['url', 'publicdate', 'rubric', 'text', 'rating']}),
        ('ip-voted', {'fields': ['liked_ip'], 'classes': ['collapse']}),
    ]

    list_filter = ['publicdate', 'rubric']
    search_fields = ['text']

admin.site.register(Anekdots, AnekdotiAdmin)
admin.site.register(Rubric)
admin.site.register(Site)
admin.site.register(Menu)
