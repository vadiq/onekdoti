from __future__ import unicode_literals

from django.db import models
from datetime import datetime
from django.utils.encoding import python_2_unicode_compatible
from ckeditor.fields import RichTextField

@python_2_unicode_compatible
class Rubric(models.Model):
    name = models.CharField(max_length=200)
    titl_for_top = models.CharField(max_length=10, blank=True)
    descr = RichTextField(blank=True)
    url = models.CharField(max_length=200, default='enter url')
    approved = models.BooleanField(default=False)
    photo = models.ImageField(blank=True)
    alt_photo = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return self.name

@python_2_unicode_compatible
class Anekdots(models.Model):
    url = models.CharField(max_length=200, default='enter url')
    publicdate = models.DateTimeField(default=datetime.now())
    rubric = models.ForeignKey(Rubric, on_delete=models.CASCADE)
    text = models.TextField()
    rating = models.IntegerField(default=0)
    liked_ip = models.TextField(blank=True)

    def __str__(self):
        return self.text


@python_2_unicode_compatible
class Site(models.Model):
    url = models.CharField(max_length=100)
    descr = models.TextField(blank=True)


    def __str__(self):
        return self.url

@python_2_unicode_compatible
class Menu(models.Model):
    name = models.CharField(max_length=50)
    url = models.CharField(max_length=80, default='enter url')
    text = RichTextField(max_length=5000, default='enter text')

    def __str__(self):
        return self.name