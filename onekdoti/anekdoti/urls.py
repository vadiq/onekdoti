from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^search', views.search, name='search'),
    url(r'^login/', views.login_ajax),
    url(r'^like/', views.like),
    url(r'^unlike/', views.unlike),
    url(r'^like-collect/', views.like_collect),
    url(r'^unlike-collect/', views.unlike_collect),
    url(r'^like-top9/', views.like_top9),
    url(r'^unlike-top9/', views.unlike_top9),
    url(r'^auth/logout/', views.logout),
    url(r'^auth/login/', views.login),
    url(r'^auth/register/', views.register),
    url(r'^add-to-korzina/', views.add_korzina),
    url(r'^remove-korzina/', views.remove_korzina),
    url(r'^add-to-top/', views.add_my_top),
    url(r'^all-rubrics', views.all_rubrics),
    url(r'^remove-top/$', views.remove_my_top),
    url(r'^my-anekdots/$', views.my_anekdots),
    url(r'^my-top-anekdots/(?P<user_top9>.+)', views.user_top9),
    url(r'^my-anekdots/(?P<user_collect>.+)$', views.user_collect),
    url(r'^my-top-anekdots/', views.my_top_anekdots),
    url(r'^(?P<menu>contacts)', views.menu),
    url(r'^(?P<menu>about-project)', views.menu),
    url(r'^anekdot/(?P<anekdor_id>.+)$', views.page_anekdot),
    url(r'^(?P<rubric>.+)$', views.rubric),
    url(r'^', views.anekdoti)
]