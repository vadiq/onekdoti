from django.contrib import auth
from .models import Menu, Anekdots, Rubric, Site
from login.models import MyTop

def context(request):
    rubrics = Rubric.objects.filter(approved=True)

    def sort_by_count_anekdots(rubr_count_anekdots):
        return len(Anekdots.objects.filter(rubric=rubr_count_anekdots))

    rubrics = sorted(rubrics, key=sort_by_count_anekdots, reverse=True)
    rubrics = rubrics[:8]
    best_anekdots_in_rubric = []
    anectods_rubric_count = []

    for rubric in rubrics:
        best_anekdot = Anekdots.objects.filter(rubric=rubric).order_by('-rating')[0]
        best_anekdots_in_rubric.append(best_anekdot)
        anectods_rubric_count.append(len(Anekdots.objects.filter(rubric=rubric)))

    for_top = zip(rubrics,best_anekdots_in_rubric,anectods_rubric_count)

    users_top = MyTop.objects.filter(anekdotki__gt=0).distinct()
    list_anekdot_top_user = []
    for user in users_top:
        best_anekdot_user = Anekdots.objects.filter(mytop__user=user.user).order_by('-rating')[0]
        list_anekdot_top_user.append(best_anekdot_user)

    users_top_anekdot = zip(users_top, list_anekdot_top_user)

    return {'menu': Menu.objects.all(),
            'rubrics': Rubric.objects.filter(approved=True),
            'site': Site.objects.filter().first(),
            'username': auth.get_user(request),
            'for_top': for_top,
            'users_top': users_top,
            'users_top_anekdot': users_top_anekdot
            }

