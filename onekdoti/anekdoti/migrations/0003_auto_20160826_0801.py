# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-08-26 08:01
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('anekdoti', '0002_auto_20160710_1323'),
    ]

    operations = [
        migrations.AddField(
            model_name='rubric',
            name='titl_for_top',
            field=models.CharField(blank=True, max_length=10),
        ),
        migrations.AlterField(
            model_name='anekdots',
            name='publicdate',
            field=models.DateTimeField(default=datetime.datetime(2016, 8, 26, 8, 1, 46, 951824)),
        ),
    ]
