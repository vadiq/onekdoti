# -*- coding: UTF-8 -*-
import os
import random
os.environ['DJANGO_SETTINGS_MODULE'] = 'onekdoti.settings'
import django
django.setup()
from anekdoti.models import Rubric


a = [u'Короткие и длинные', u'Смешные', u'Забавные', u'В большой коллекции', u'В разделе', u'В рубрике']
b = [u'обязательно рассмешат читателя.', u'всегда поднимут читателю настроение.', u'заставят улыбнуться любителей юмора.']
c = [u'Одна хорошая шутка способна зарядить энергией на весь день.', u'Минута смеха может дать сил на целый день.']
d = [u'Выбирайте самые лучшие', u'Соберите самые смешные']
e = [u'и создайте свою коллекцию, с которой можно будет удобно поделиться с друзьями.', u'и добавьте их в свою коллекцию, с которой можно будет удобно поделиться с друзьями.']
j = [u'Кстати, у нас можно голосовать за любой анекдот, что поможет показывать пользователям только лучшие смешные шутки.', u'Еще у нас '
                                                                                                                            u'можно отдать свой голос за анекдот, тем самым помочь проекту сортировать только самые крутые смешные истории.']
z = [u'Из любимой коллекции можно создать свой ТОП-9 анекдотов и поделиться им с друзьями.', u'Из созданной коллекции можно выбрать свой ТОП-9 анекдотов и показать его своим друзьям.']
i = [u'Возможно в нем окажутся некоторые', u'Может в него также попадут']

rubriks = Rubric.objects.filter(approved=True)
for rubr in rubriks:
    if len(rubr.descr) < 100:
        rubr.descr = '<p>%s %s %s </p> <p>%s </p> <p>%s %s %s</p> <p>%s</p> <p>%s %s %s?</p>' % (random.choice(a), rubr.name.lower(), random.choice(b),
                                               random.choice(c), random.choice(d), rubr.name.lower(), random.choice(e), random.choice(j), random.choice(z), random.choice(i), rubr.name.lower())
        rubr.save()
    else:
        rubr.descr = rubr.descr+'<p>%s %s %s </p> <p>%s </p> <p>%s %s %s</p> <p>%s</p> <p>%s %s %s?</p>' % (random.choice(a), rubr.name.lower(), random.choice(b),
                                                                                                            random.choice(c), random.choice(d), rubr.name.lower(), random.choice(e), random.choice(j), random.choice(z), random.choice(i), rubr.name.lower())
        rubr.save()